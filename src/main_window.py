import tkinter as tk

import tkinter.messagebox as msg_box

from db_setup import Base, Users, Lists, Words, db_name

from frames import Frm_Edit, Frm_Lerning, Frm_Select, Frm_Login, Frm_In

from sqlalchemy import create_engine
from sqlalchemy.orm import Session, query

import time

window = tk.Tk()

window.columnconfigure(0, pad=3, weight=1)
window.rowconfigure(0, pad=3, weight=1)

window.geometry('600x300+0+0')

# Создание и последуещее прикрепление на форму элементов:
"""
    Frm_In
    Здесь настраиваю расположение форму и работу кнопок.
"""
frm_in = Frm_In(window)
window.config(menu = frm_in.menu)

def move_login():
    frm_move('in', 'login')

frm_in.btn_login.configure(command=move_login)

frm_in.grid(row=0, column=0)

"""
    Frm_Iogin
    Здесь настраиваю расположение форму и работу кнопок.
"""
frm_login = Frm_Login(window)

# Лезем в безу данных, чтобы проверить есть ли такой пользаватель, но а пока:
def login_btn_login():
    engine = create_engine(db_name, echo=True)
    Base.metadata.bind = engine
    session = Session(bind = engine)

    query = session.query(Users).filter(Users.name == frm_login.entry_name.get())
    anser_list = query.all()
    count = query.count()

    if(count == 0):
        frm_login.lbl_msg.configure( text = 'this name or password not correct')
    elif(count > 1):
        print('somsing go wrong. More 1 user was wound')
        frm_login.lbl_msg.configure( text = 'somthing go wrang. Try again later')
    else:
        user = anser_list[0]
        if(user.password != frm_login.entry_password.get()):
            frm_login.lbl_msg.configure( text = 'this name or password not correct')
        else:
            my_list = session.query(Lists).all()
            for item in my_list:
                print(item)

            lists = session.query(Lists).filter(Lists.user_id == user.id).all()
            for item in lists:
                frm_select_list.listbox.insert(tk.END, "%s" % item.name)

            frm_select_list.curent_user = user
            frm_move('login', 'select')

def login_btn_back():
    frm_select_list.delete_list()
    frm_move('login', 'in')

frm_login.menu.add_command(label="Back", command=login_btn_back)
frm_login.btn_submit.configure(command=login_btn_login)

"""
    Frm_Select
    Здесь настраиваю расположение форму и работу кнопок.
"""

frm_select_list = Frm_Select(window)

frm_select_list.grid_remove()


# Dозвращает во фрейм аутентификации (Frm_Login)
def select_btn_back():
    answer = msg_box.askyesnocancel("Exit", "Exit, really?", parent=window)
    if(answer != None):
        if(answer):
            frm_select_list.delete_list()
            frm_move('select', 'in')

# Нужена, чтобы перейти в фрейм создания нового списка слов
def select_btn_add():
    frm_edit_list.set_name('Новое имя')
    frm_edit_list.curent_list = None
    frm_edit_list.curent_user = frm_select_list.curent_user

    frm_move('select', 'edit')

# Нужна чтобы полностью удалить список слов.
def select_btn_delete():
    cur_tuple = frm_select_list.listbox.curselection()

    if(cur_tuple == ()):
        return

    answer = msg_box.askyesnocancel('Delition', 'Really?', parent=window)

    if(answer != None):
        if(answer):
            list_index = frm_select_list.listbox.curselection()
            list_name = frm_select_list.listbox.get(list_index)

            engine = create_engine(db_name, echo=True)
            Base.metadata.bind = engine
            session = Session(bind = engine)

            del_list = session.query(Lists).filter(Lists.name == list_name).one()
            session.query(Words).filter(Words.list_id == del_list.id).delete()
            session.delete(del_list)
            frm_select_list.listbox.delete(list_index)
            session.commit()

# Нужна, чтобы выбрать нужный список слов.
def select_btn_chose():
    cur_tuple = frm_select_list.listbox.curselection()

    if(cur_tuple != ()):
        list_name = frm_select_list.listbox.get(cur_tuple)

        engine = create_engine(db_name, echo=True)
        Base.metadata.bind = engine
        session = Session(bind = engine)

        curent_list = session.query(Lists).filter(Lists.name == list_name).one()
        curent_list_words = session.query(Words).filter(Words.list_id == curent_list.id).all()
        frm_learning.teacher.setList(curent_list_words)
        frm_learning.quest_text.set(frm_learning.teacher.get_word())#!!!!!!!!!!!!!!!!!!!!!

        frm_move('select', 'learning')

# Нужена, чтобы перейти в режим изменения выбранного списка слов.
def select_btn_edit():
    cur_tuple = frm_select_list.listbox.curselection()

    if(cur_tuple != ()):
        name = frm_select_list.listbox.get(cur_tuple)

        engine = create_engine(db_name, echo=True)
        Base.metadata.bind = engine
        session = Session(bind = engine)
        
        frm_edit_list.set_name(name)
        curent_list = session.query(Lists).filter(Lists.name == name).one()

        word_list = session.query(Words).filter(Words.list_id == curent_list.id).all()
        for word in word_list:
            frm_edit_list.word_box.insert(
                '', 
                tk.END, 
                text=word.russion, 
                value=(word.english, word.id, 'False'))

        frm_edit_list.curent_list = curent_list
        frm_edit_list.curent_user = frm_select_list.curent_user

        frm_move('select', 'edit')

def delite_progress():
    answer = msg_box.askyesnocancel('Delete', 'Really?', parent=window)
    if(answer != None):
        if(answer):
            cur_tuple = frm_select_list.listbox.curselection()

            if(cur_tuple != ()):
                list_name = frm_select_list.listbox.get(cur_tuple)

                engine = create_engine(db_name, echo=True)
                Base.metadata.bind = engine
                session = Session(bind = engine)

                curent_list = session.query(Lists).filter(Lists.name == list_name).one()
                curent_list_words = session.query(Words).filter(Words.list_id == curent_list.id).\
                    update({Words.progress: 0}, synchronize_session=False)
                session.commit()

# Навешивание действий на кнопки

# Меню
frm_select_list.menu.add_command(label="Back", command=select_btn_back)
frm_select_list.menu.add_separator()
frm_select_list.menu.add_command(label="Add", command=select_btn_add)
frm_select_list.menu.add_separator()
frm_select_list.menu.add_command(label="Delete", command=select_btn_delete)
frm_select_list.menu.add_separator()
frm_select_list.menu.add_command(label="Delite progress", command=delite_progress)
frm_select_list.menu.add_separator()
frm_select_list.menu.add_command(label="Edit", command=select_btn_edit)
frm_select_list.menu.add_separator()
frm_select_list.menu.add_command(label="Chose", command=select_btn_chose)

"""
    Frm_Edit
    Здесь настраиваю расположение форму и работу кнопок
"""
frm_edit_list = Frm_Edit(window)
frm_edit_list.grid_remove()

# Добавляет новое слово в список
def edit_btn_add():
    rus = frm_edit_list.rus_text.get()
    eng = frm_edit_list.eng_text.get()

    frm_edit_list.word_box.insert('', tk.END, text = rus, value=(eng, -1, 'False'))
    rus = frm_edit_list.rus_text.set('')
    eng = frm_edit_list.eng_text.set('')


# Сохраняет изменения и выходить в фрейм выбора списков слов (Frm_Select)
def edit_btn_save():
    engine = create_engine(db_name, echo=True)
    Base.metadata.bind = engine
    session = Session(bind = engine)

    if(frm_edit_list.curent_list == None):
        #По хорошему проверка на уникальность!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        new_list = Lists(frm_edit_list.name_text.get(), frm_edit_list.curent_user.id)
        session.add(new_list)
        session.commit()
        frm_edit_list.curent_list = session.query(Lists).filter(Lists.name == frm_edit_list.name_text.get()).one()
        frm_select_list.listbox.insert(tk.END, "%s" % frm_edit_list.curent_list.name)

    for index in frm_edit_list.word_box.get_children(''):
        item = frm_edit_list.word_box.item(index)
        if(item['values'][1] != -1 and item['values'][2] == 'True'):
            session.query(Words).filter(Words.id == item['values'][1]).\
                update({
                    Words.russion: item['text'], 
                    Words.english: item['values'][1],
                    Words.progress: 0}, synchronize_session=False)
        elif(item['values'][1] == -1 and item['text'] != ''):
            new_word = Words(item['text'], item['values'][0], frm_edit_list.curent_list.id)
            session.add(new_word)
        elif(item['text'] == '' and item['values'][1] != -1):
            session.query(Words).filter(Words.id == item['values'][1]).delete()
    session.commit()

    frm_move('edit', 'select')

# Перемещает на фрейм выбора списков слов (Frm_Select).
def edit_btn_back():
    answer = msg_box.askyesnocancel('Yes/No', 'Wana save?', parent = window)

    if (answer != None):
        if(answer):
            edit_btn_save()
        else:
            frm_move('edit', 'select')


# Удаляет слово из списка
def edit_btn_delite():
    answer = msg_box.askyesnocancel('Delete', 'Really?', parent=window)
    if(answer != None):
        if(answer):
            for selection in frm_edit_list.word_box.selection():
                _item = frm_edit_list.word_box.item(selection)
                frm_edit_list.word_box.item(selection, text='', value=('', _item['values'][1], ''))
                frm_edit_list.word_box.move(selection, '', tk.END)

# Изменяет выбранное слово
def edit_btn_edit():
    for selection in frm_edit_list.word_box.selection():
        item = frm_edit_list.word_box.item(selection)
        this_value = (frm_edit_list.eng_text.get(), item['values'][1], 'True')
        frm_edit_list.word_box.item(
            selection, 
            text = frm_edit_list.rus_text.get(), 
            values= this_value)

# Навешивание действий на кнопки
frm_edit_list.menu.add_command(label="Back", command=edit_btn_back)
frm_edit_list.menu.add_separator()
frm_edit_list.menu.add_command(label="Add", command=edit_btn_add)
frm_edit_list.menu.add_separator()
frm_edit_list.menu.add_command(label="Delete", command=edit_btn_delite)
frm_edit_list.menu.add_separator()
frm_edit_list.menu.add_command(label="Edit", command=edit_btn_edit)
frm_edit_list.menu.add_separator()
frm_edit_list.menu.add_command(label="Save", command=edit_btn_save)

frm_edit_list.btn_add.configure(command=edit_btn_add)
frm_edit_list.btn_save.configure(command=edit_btn_save)
frm_edit_list.btn_delite.configure(command=edit_btn_delite)
frm_edit_list.btn_edit.configure(command=edit_btn_edit)

"""
    Frm_Lerning
    Здесь настраиваю расположение форму и работу кнопок.
"""
frm_learning = Frm_Lerning(window)
frm_learning.grid_remove()

# Отправляет на фрейм выбора списка слов (Frm_Select).
#            Перед переходом она спрашивает нужно ли сохранить прогресс.
def learning_btn_back():
    answer = msg_box.askyesnocancel('Yes/No', 'Wana save progress?', parent = window)
    if (answer != None):
        if(answer):
            engine = create_engine(db_name, echo=True)
            Base.metadata.bind = engine
            session = Session(bind = engine)

            for word in frm_learning.teacher.list_words:
                session.query(Words).filter(Words.id == word.id).\
                    update({Words.progress: word.progress}, synchronize_session=False)
            session.commit()

        frm_move('learning', 'select')

# Нужна чтобы отправлять ответ на проверку. 
#            Отправляется ревью (правильность и ошибки).
#            Нажимается кнопка ещё раз и отправляется новое слово.
def learning_btn_send():
    if(frm_learning.btn_send_state == 'send'):
        anser = frm_learning.teacher.send_on_check(frm_learning.ans_text.get())
        if(anser == "#end#"):
            frm_learning.btn_send_state = 'end'
            frm_learning.ans_text.set('Красава')
            frm_learning.quest_text.set('Красава')
            return
        frm_learning.ans_text.set(anser)
        frm_learning.btn_send_state = 'watch'
    elif(frm_learning.btn_send_state == 'watch'):
        frm_learning.quest_text.set(frm_learning.teacher.get_word())
        frm_learning.ans_text.set('')
        frm_learning.btn_send_state = 'send'
    elif(frm_learning.btn_send_state == 'end'):
        engine = create_engine(db_name, echo=True)
        Base.metadata.bind = engine
        session = Session(bind = engine)

        for word in frm_learning.teacher.list_words:
                session.query(Words).filter(Words.id == word.id).\
                    update({Words.progress: 0}, synchronize_session=False)
        session.commit()
        frm_learning.btn_send_state = 'send'
        
        frm_move('learning', 'select')

# Навешивание действий на кнопки
frm_learning.btn_send.configure(command=learning_btn_send)

frm_learning.menu.add_command(label="Back", command=learning_btn_back)

def in_out():
    frm_in.grid_remove()

def login_out():
    frm_login.set_empty()
    frm_login.grid_remove()

def select_out():
    frm_select_list.grid_remove()

def edit_out():
    frm_edit_list.set_empty()
    frm_edit_list.grid_remove()
    
def learning_out():
    frm_learning.set_empty()
    frm_learning.grid_remove()

def in_in():
    frm_in.grid(row=0, column=0)
    window.config(menu = frm_in.menu)
    
def login_in():
    frm_login.grid(row=0, column=0, sticky='ns')
    window.config(menu = frm_login.menu)

def select_in():
    frm_select_list.grid(row=0, column=0, sticky='nswe')
    window.config(menu = frm_select_list.menu)

def edit_in():
    frm_edit_list.grid(row=0, column=0, sticky='nswe')
    window.config(menu = frm_edit_list.menu)

def learning_in():
    frm_learning.grid(row=0, column=0, sticky='nswe')
    window.config(menu = frm_learning.menu)

move_out_switch = {
        'in': in_out,
        'login': login_out,
        'select': select_out,
        'edit': edit_out,
        'learning': learning_out,
    }

move_in_switch = {
        'in': in_in,
        'login': login_in,
        'select': select_in,
        'edit': edit_in,
        'learning': learning_in,
    }

def frm_move(frm_out, frm_in):
    """
        Нужно для переключения фреймов.
        parame frm_out: фрейм, которыйнужно убрать
        parame frm_in:  фрейм, котороый нужно поместить в поле видимости
        возможные значения (login, select, edit, learning)
    """
    try:
        move_out_switch[frm_out]()
        move_in_switch[frm_in]()
    except KeyError as e:
        print(e)

window.mainloop()