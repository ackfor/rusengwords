import tkinter as tk

import tkinter.ttk as ttk

from db_setup import Base, Users, db_name

from sqlalchemy import create_engine

from sqlalchemy.orm import Session, query

class Reg_window(tk.Tk):
    """
        Это окно нужено для регистрации.
        lbl_msg нужен для вывода сообщений ("Такого пользователя уже есть" и т.д)
        entry_name поле для ввода имени
        password поле для ввода пароля
        confirm поле для подтверждения пороля
        btn_register пытается зарегистрировать нового пользователя
    """
    def __init__(self, screenName=None, baseName=None, className='Tk',
        useTk=True, sync=False, use=None):

        super().__init__(screenName=None, baseName=None, className='Tk', 
            useTk=True, sync=False, use=None)

        self.name = ''

        self.reg_name_frm = Reg_Name_Frm(self)
        self.reg_password_frm = Reg_Password_Frm(self)
        
        self.reg_password_frm.btn_register.configure(command=self.__put_user_data)
        self.reg_name_frm.btn_next.configure(command=self.__is_name_exist)
        self.reg_password_frm.btn_back.configure(command=self.__btn_back)

        self.move_out_switch = {
            'name': self.name_out,
            'password': self.password_out
        }
        self.move_in_switch = {
                'name': self.name_in,
                'password': self.password_in
        }
        #reg_login_frm.grid(row=0, column=0, sticky='nswe')

        self.UI()

    def __is_name_exist(self):
        engine = create_engine(db_name, echo=True)
        Base.metadata.bind = engine
        session = Session(bind = engine)
        is_exist = session.query(Users).filter(Users.name == self.reg_name_frm.entry_name.get()).count()

        if(is_exist != 0):
            self.reg_name_frm.lbl_msg.configure(text="Такой пользователь уже есть")
            return

        self.name = self.reg_name_frm.entry_name.get()
        self.reg_password_frm.lbl_msg.configure(
                text="%s, введите пароль" % self.name)
        self.frm_move('name', 'password')

    def __btn_back(self):
        self.frm_move('password', 'name')

    def __put_user_data(self):
        if(self.reg_password_frm.str_password.get() != self.reg_password_frm.str_confirm.get()):
            self.reg_password_frm.lbl_msg.configure(
                text="%s, Пароли не совпадают" % self.reg_name_frm.str_name.get())
            return

        engine = create_engine(db_name, echo=True)
        Base.metadata.bind = engine
        session = Session(bind = engine)

        self.reg_password_frm.lbl_msg.configure(text="")
        new_user = Users(self.reg_name_frm.str_name.get(), self.reg_password_frm.str_password.get())
        session.add(new_user)
        session.commit()

        self.quit()

    def UI(self):
        self.geometry('320x150')

        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1)
        self.reg_name_frm.grid(row=0, column=0)

    def name_in(self):
        self.reg_name_frm.str_name.set(self.name + '1234')
        self.reg_name_frm.grid(row=0, column=0, sticky='nswe')

    def password_in(self):
        self.reg_password_frm.grid(row=0, column=0, sticky='nswe')

    def name_out(self):
        self.reg_name_frm.grid_remove()

    def password_out(self):
        self.reg_password_frm.grid_remove()
        self.reg_password_frm.str_password.set('')
        self.reg_password_frm.str_confirm.set('')

    def frm_move(self, frm_out, frm_in):
        """
            Нужно для переключения фреймов.
            parame frm_out: фрейм, которыйнужно убрать
            parame frm_in:  фрейм, котороый нужно поместить в поле видимости
            возможные значения (login, select, edit, learning)
        """
        try:
            self.move_out_switch[frm_out]()
            self.move_in_switch[frm_in]()
        except KeyError as e:
            print(e)

class Reg_Name_Frm(tk.Frame):
    """
    """
    def __init__(self, master=None, cnf={}, **kw):
        super().__init__(master, cnf, **kw)

        self.lbl_msg = tk.Label(self, text='')
        self.lbl_name = tk.Label(self, text='Имя')
        self.str_name = tk.StringVar()
        self.entry_name = tk.Entry(self, fg = "black", textvariable=self.str_name)
        self.btn_next = tk.Button(self, text='Next', width = 20)

        self.UI()

    def UI(self):
        self.rowconfigure(0, pad=10)
        self.rowconfigure(1, pad=30)
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=15)
            
        self.lbl_msg.grid(row=0, column=0, columnspan=2)
        self.lbl_name.grid(row=1, column=0, sticky='e', padx=(20, 5))
        self.entry_name.grid(row=1, column=1, sticky='w', padx=(5, 0))
        self.btn_next.grid(row=2, column=1, sticky="w")

class Reg_Password_Frm(tk.Frame):
    """
    """
    def __init__(self, master=None, cnf={}, **kw):
        super().__init__(master, cnf, **kw)

        self.lbl_msg = tk.Label(self)
        self.lbl_password = tk.Label(master = self, text='Пароль')
        self.str_password = tk.StringVar()
        self.password = tk.Entry(self, fg = "black", textvariable=self.str_password)
        self.lbl_confirm = tk.Label(master = self, text='Подтверждение')
        self.str_confirm = tk.StringVar()
        self.confirm = tk.Entry(self, fg = "black", textvariable=self.str_confirm)
        self.__frm_for_btn = tk.Frame(self)
        self.btn_back = tk.Button(self.__frm_for_btn, text = 'Back', width = 20)
        self.btn_register = tk.Button(self.__frm_for_btn, text = 'Register', width = 20)

        self.UI()
        
    def UI(self):
        self.rowconfigure(0, pad=10)
        self.rowconfigure(3, pad=30)
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=15)

        self.lbl_msg.grid(row=0, column=0, columnspan=2)
        self.lbl_password.grid(row=1, column=0, sticky='e', padx=(0,5))
        self.password.grid(row=1, column=1, sticky='w', padx=(5,0))
        self.lbl_confirm.grid(row=2, column=0, sticky='e', padx=(0,5))
        self.confirm.grid(row=2, column=1, sticky='w', padx=(5,0))
        self.__frm_for_btn.grid(row=3, column=0, columnspan=2, pady=(15,0))
        #frm_for_btn
        self.btn_back.grid(row=0, column=0)
        self.btn_register.grid(row=0, column=1)