from sqlalchemy import create_engine
from sqlalchemy import Table, Column, Integer, String, MetaData, ForeignKey

from sqlalchemy.orm import relationship, backref

from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()

db_name = "sqlite:///test.db"

"""
    Таблица, в которой находится список всех
    зарегистрировавшиеся пользователи.
"""
class Users (Base):
    __tablename__ = 'user'

    def __init__(self, name: str, password: str):
        self.name = name
        self.password = password

    def __repr__(self):
        return "<User(Name: (%s), Password: (%s))>\n" % (self.name, self.password)

    id = Column(Integer, primary_key = True)
    name = Column(String)
    password = Column(String)

"""
    Таблица, в которой находятся спиисок названий списков слов.
"""
class Lists(Base):
    __tablename__ = "lists"

    def __init__(self, name: str, user_id: int):
        self.name = name
        self.user_id = user_id

    def __repr__(self):
        return "<id: %s, name: %s, user_id: %s>" % (self.id, self.name, self.user_id)

    id = Column(Integer, primary_key = True)
    name = Column(String)

    # Пусть, если удаляется user то удаляется, все его списки
    user_id = Column(Integer, ForeignKey("user.id", ondelete="CASCADE"))

"""
    Таблица, в которой находятся список слов.
    [col(rus), col(eng)]
"""
class Words(Base):
    __tablename__ = 'words'

    def __init__(self, russion: str, english: str, list_id: int):
        self.russion = russion
        self.english = english
        self.progress = 0
        self.list_id = list_id

    def __repr__(self):
        return "<word: %s translation: %s>" % (self.order, self.translation)

    id = Column(Integer, primary_key = True)
    russion = Column(String)
    english  = Column(String)
    progress = Column(Integer)

    #Отношение со списками
    list_id = Column(Integer, ForeignKey("lists.id", ondelete="CASCADE"))

engine = create_engine(db_name, echo=True)
Base.metadata.create_all(engine)