import tkinter as tk
import tkinter.ttk as ttk

from db_setup import Base, Users, db_name

from sqlalchemy import create_engine
from sqlalchemy.orm import Session, query

from teacher import Teacher

from register_window import Reg_window

class Frm_In(tk.Frame):
    """
    """
    def __init__(self, master=None, cnf={}, **kw):
        super().__init__(master, cnf, **kw)

        self.menu = tk.Menu(self)
        self.btn_login = tk.Button(
            self,
            text = "Login",
            fg = "white",
            bg = "black",
            width=25
        )
        self.btn_register = tk.Button(
            self,
            text = 'Register',
            width=25,
            command=lambda: Reg_window().mainloop()
        )

        self.UI()

    def UI(self):
        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1)

        self.btn_login.grid(column=0, row=0, sticky='e')
        self.btn_register.grid(column=1, row=0, sticky='w')


class Frm_Login(tk.Frame):
    """
        Этот фрейм нужен для авторизации.
        lbl_msg нужен для вывода сообщений ("Такого пользователя нет" и т.д)
        entry_name поле для ввода имени
        btn_login После вввода имени она проверяет есть ли такой в базе данных
        btn_register Выводит форму для регистрации
    """
    def __init__(self, master=None, cnf={}, **kw):
        super().__init__(master, cnf, **kw)

        # Делаю уже саму форму
        self.menu = tk.Menu(self)
        self.lbl_msg = tk.Label(self)

        self.frm_name = tk.Frame(self)
        self.lbl_name = tk.Label(self.frm_name, text='Имя', width=10)
        self.entry_name = tk.Entry(self.frm_name, width=40)

        self.frm_password = tk.Frame(self)
        self.lbl_password = tk.Label(self.frm_password, text='Пароль', width=10)
        self.entry_password = tk.Entry(self.frm_password, width=40)
        self.btn_submit = tk.Button(
            self,
            text = "Submit",
            fg = "white",
            bg = "black",
            width=25)

        self.UI()

    def UI(self):
        self.rowconfigure(0, weight=1)
        self.rowconfigure(3, weight=10)

        self.lbl_msg.grid(column=0, row=0)
        self.frm_name.grid(column=0, row=1)
        self.frm_password.grid(column=0, row=2)
        self.btn_submit.grid(column=0, row=3)

        #frm_name
        self.lbl_name.grid(column=0, row=0, sticky='w')
        self.entry_name.grid(column=1, row=0, sticky='e', columnspan=2, padx=(0, 10))

        #frm_password
        self.lbl_password.grid(column=0, row=0, sticky='w')
        self.entry_password.grid(column=1, row=0, sticky='e', columnspan=2, padx=(0, 10))

    def set_empty(self):
        self.lbl_msg.configure(text = '')
        self.entry_name.delete(0, 'end')
        self.entry_password.delete(0, 'end')

class Frm_Select (tk.Frame):
    """
    В этом фрейме (Frm_Select) отображаются все готоые списки слов.
    listbox Здесь выводится списки слов
    btn_chose нужна, чтобы выбрать нужный список слов
    btn_edit нужена, чтобы перейти в режим изменения выбранного списка слов
    btn_add нужена, чтобы перейти в фрейм создания нового списка слов
    btn_delite нужна чтобы полностью удалить список вместе с прогрессом
    btn_back возвращает во фрейм аутентификации (Frm_Login)
    """
    def __init__(self, master=None, cnf={}, **kw):
        super().__init__(master, cnf, **kw)

        self.curent_user = None

        #Лучше черз threewiev потому что там можно делать скрытые столбцы.
        self.menu = tk.Menu(self)
        self.listbox = tk.Listbox(self)

        self.UI()

    def UI(self):
        self.columnconfigure(0, pad=3, weight=1)
        self.rowconfigure(0, pad=3, weight=1)
        
        self.listbox.grid(row=0, column=0, sticky='nswe', padx=10, pady=(10,60))


    def delete_list(self):
        self.listbox.delete(0, tk.END)

class Frm_Edit(tk.Frame):
    """
        В этом фрейме происходит создание и редактирование списка слов.
        Создание - редактирование пустого списка.
        word_box выводится список слов в формате ("на русском" "на английском" "прогресс")
        rus_entry поле для изменения русского слова
        eng_entry поле для изменения английского слова
        btn_delite удаляет слово из списка
        btn_edit изменяет выбранное слово
        btn_add добавляет новое слово в список
        btn_save сохраняет изменения и выходить в фрейм выбора списков слов (Frm_Select)
        btn_back перемещает на фрейм выбора списков слов (Frm_Select).
            Спрашивает сохранить ли сделанные изменения.
    """
    def __init__(self, master=None, cnf={}, **kw):
        super().__init__(master, cnf, **kw)

        self.curent_user = None
        self.curent_list = None

        self.menu = tk.Menu(self)
        self.lbl_name = tk.Label(self, text='Имя')
        
        self.name_text = tk.StringVar()
        self.input_name = tk.Entry(self, textvariable=self.name_text)

        self.lbl_rus_input = tk.Label(self, text='Русский')
        self.rus_text = tk.StringVar()
        self.rus_entry = tk.Entry(self, textvariable=self.rus_text)

        self.lbl_eng_input = tk.Label(self, text='Английский')
        self.eng_text = tk.StringVar()
        self.eng_entry = tk.Entry(self, textvariable=self.eng_text)
        
        self.word_box = ttk.Treeview(master=self)
        self.word_box['columns'] = ('eng', 'ID', 'Edited')
        self.word_box.heading('#0', text='Русский')
        self.word_box.heading('eng', text='Английский')
        self.word_box.column('#0', stretch=0, minwidth=50, width=100)
        self.word_box.column('eng', stretch=0, minwidth=50, width=100)
        self.word_box['displaycolumns'] = ('eng')
        self.word_box.bind("<<TreeviewSelect>>", self.set_selection)
        
        ysb = ttk.Scrollbar(self, orient=tk.VERTICAL, command=self.word_box.yview)
        self.word_box.configure(yscroll=ysb.set)

        self.btn_edit = tk.Button(self, text = 'Edit')
        self.btn_add = tk.Button(self, text = 'Add')
        self.btn_save = tk.Button(self, text = 'Save')
        self.btn_delite = tk.Button(self, text = 'Delite')

        self.UI()

    def UI(self):
        self.rowconfigure(0)
        self.rowconfigure(1)
        self.rowconfigure(2)
        self.rowconfigure(3)
        self.rowconfigure(4, weight=1)
        self.rowconfigure(5, weight=1)
        
        self.columnconfigure(0)
        self.columnconfigure(1, minsize=350, weight=1)
        self.columnconfigure(2)
        self.columnconfigure(3)

        self.lbl_name.grid(row=0, column=0, sticky='w')
        self.input_name.grid(row=0, column=1, sticky='we', padx=(5, 0))
        self.lbl_rus_input.grid(row=1, column=2, sticky='e')
        self.rus_entry.grid(row=1, column=3, sticky='we', padx=(5, 0))
        self.lbl_eng_input.grid(row=2, column=2, sticky='e')
        self.eng_entry.grid(row=2, column=3, sticky='we', padx=(5, 0))
        self.word_box.grid(row=1, column=0, columnspan=2, rowspan=5, sticky='nswe')
        self.btn_edit.grid(row=4, column=2, sticky='wes', padx=(0,5), pady=(0,5))
        self.btn_add.grid(row=4, column=3, sticky='wes', padx=(5,0), pady=(0,5))
        self.btn_save.grid(row=5, column=2, sticky='wen', padx=(0,5), pady=(5,0))
        self.btn_delite.grid(row=5, column=3, sticky='wen', padx=(5,0), pady=(5,0))


    def set_selection(self, event):
        for selection in self.word_box.selection():
                item = self.word_box.item(selection)
                rus_word = item['text']
                eng_word = item['values'][0]
        self.rus_text.set(rus_word)
        self.eng_text.set(eng_word)

    def set_name(self, text):
        self.name_text.set(text)

    def set_empty(self):
        self.rus_text.set('')
        self.eng_text.set('')
        for item in self.word_box.get_children(''):
            self.word_box.delete(item)


class Frm_Lerning(tk.Frame):
    """
        В этом фрейме происходит обучение. 
        В label_question классо Teacher помещается слово на русском 
        В label_answer вводится слово на английском
        btn_send нужна чтобы отправлять ответ на проверку. 
            Отправляется ревью (правильность и ошибки).
            Нажимается кнопка ещё раз и отправляется новое слово.
        btn_back отправляет на фрейм выбора списка слов (Frm_Select).
            Перед переходом она спрашивает нужно ли сохранить прогресс.
    """
    def __init__(self, master=None, cnf={}, **kw):
        super().__init__(master, cnf, **kw)

        self.teacher = Teacher()

        self.menu = tk.Menu(self)
        self.btn_send_state = 'send' # (send, watch, end)

        self.quest_text = tk.StringVar()
        self.label_question = tk.Entry(
            self,
            fg = "black",
            width = 30,
            textvariable=self.quest_text
        )

        self.ans_text = tk.StringVar()
        self.label_answer = tk.Entry(
            self,
            fg = "black",
            width = 30,
            textvariable=self.ans_text
        )

        self.lbl_msg = tk.Label(self)
        # 'Ok' потому что нужно будет 2 раза на неё нажать. 
        # Чтобы отправить на проверку (потом появятся исправления) 
        # и после ревью нажать, чтобы появилось новое слово.
        self.btn_send = tk.Button(self, width=15, text = 'Ok')

        self.UI()

    def UI(self):
        self.rowconfigure(0)
        self.rowconfigure(1)
        
        self.columnconfigure(0, weight=1)
        self.columnconfigure(1, weight=1)

        self.label_question.grid(row=0, column=0, sticky='e')
        self.label_answer.grid(row=1, column=0, sticky='e')
        self.lbl_msg.grid(row=0, column=1)
        self.btn_send.grid(row=1, column=1, padx=(20,0), sticky='w')
        
    def set_empty(self):
        self.quest_text.set('')
        self.ans_text.set('')



        """
            sequence: <modifier-type-detail>
            modifier - Модификаторы являются опциональными и 
            позволяют задать дополнительные комбинации для общего типа события:
                Shift – когда пользователь нажимает клавишу Shift.
                Alt – когда пользователь нажимает клавишу Alt.
                Control – когда пользователь нажимает клавишу Control.
                Lock – когда пользователь нажимает клавишу Lock.
                Shift – когда пользователь нажимает клавишу Shift.
                Shift – когда пользователь нажимает клавишу Shift lock.
                Double – когда событие происходит дважды подряд.
                Triple – когда событие происходит трижды подряд.

            type - Типы события определяют общий тип события:
                ButtonPress или Button – события, которые генерируются при нажатии кнопки мыши.
                ButtonRelease – событие, когда кнопка мыши отпускается.
                Enter – событие при перемещении мыши на виджет.
                Leave – событие, когда мышь покидает область виджета.
                FocusIn – событие, когда фокус ввода попадает в виджет.
                FocusOut – событие, когда виджет теряет фокус ввода.
                KeyPress или Key – событие для нажатия кнопки.
                KeyRelease – событие для отпущенной кнопки.
                Motion – событие при перемещении мыши.

            detail – также опциональный параметр, 
            который отвечает за определение конкретной клавиши или кнопки:
                Для событий мыши 
                    1 — это левая кнопка, 
                    2 — средняя, 
                    3 — правая
                Для событий клавиатуры используются сами клавиши. 
                Если это специальные клавиши, то используется специальный символ: 
                    enter, Tab, Esc, up, down, right, left, 
                    Backspace и функциональные клавиши (от F1 до F12).

            detail – также опциональный параметр, который отвечает за определение конкретной клавиши или кнопки:

            Функция callback принимает параметр события. Для событий мыши это следующие атрибуты:
                x и y – текущее положение мыши в пикселях
                x_root и y_root — то же, что и x или y, но относительно верхнего левого угла экрана
                num – номер кнопки мыши
                Для клавиш клавиатуры это следующие атрибуты:

                char – нажатая клавиша в виде строки
                keysym – символ нажатой клавиши
                keycode – код нажатой клавиши
                В обоих случаях у события есть атрибут widget, 
                    ссылающийся на экземпляр, который сгенерировал событие и type, определяющий тип события.
        """
