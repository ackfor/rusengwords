from sqlalchemy import create_engine
from sqlalchemy.orm import Session, query

from db_setup import Base, Users, Lists, Words, db_name

engine = create_engine(db_name, echo=True)
Base.metadata.bind = engine
session = Session(bind = engine)

session.query(Words).update({Words.progress: 0}, synchronize_session=False)

session.commit()