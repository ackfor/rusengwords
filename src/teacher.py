import random

from db_setup import Lists, Words

class Teacher:
    __ROUND = 0
    __MAX_LENGTH_ROUND = 10
    
    #list_word = [key(rus): value(eng)]
    def __init__(self, list_words = None):
        self.__curent_word = ''
        self.end_lerning = True
        self.list_words = None

        if(list_words != None):
            self.setList(list_words)

    def setList(self, list_words):
        if(list_words == None):
            print('Надо ексептион какой-нибудь')
            return
        
        self.list_words = list_words
        self.end_lerning = False    
        self.__curent_word = random.choice(self.list_words)
    
    def get_word(self):
        return self.__curent_word.russion


    def is_end(self):
        for word in self.list_words:
            if(word.progress < 5): # Парог обучения
                return False
        return True 

    """
        Отправляет слово напроверку.
        Получает сообщение, если слово написано правилно: "Правильно"
        неправильно: "(введенное слово) - не правильно
                      (слово   словаря) - правильно"
    """
    def send_on_check(self, eng_transate):
        if(self.__curent_word.english == eng_transate):
            self.__curent_word.progress += 1
            if(self.is_end()):
                self.__curent_word = None
                return "#end#"
            prog = self.__curent_word.progress
            while (True):
                self.__curent_word = random.choice(self.list_words)
                if(self.__curent_word.progress < 5):
                    break
            return "Правильно [%s]" % prog
        else:
            # __curent_word не меняю, чтобы слово спрашивал до тех пор, 
            # пока правильно не ответит
            if(self.is_end()):
                self.__curent_word = None
                return "#end#"
            self.__curent_word.progress = 0
            return "- %s [%s]" % (eng_transate, self.__curent_word.english) 
    

    
        
            


        